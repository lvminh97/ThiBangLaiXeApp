import numpy as np

#Interpolated AP - PASCAL VOC 2007 eval methoddd
# Công thức: https://jonathan-hui.medium.com/map-mean-average-precision-for-object-detection-45c121a31173

def compute_average_precision(precision, recall):
    ap = 0.
    for t in np.arange(0., 1.1, 0.1):
        if np.sum(recall >= t) == 0:
            p = 0
        else:
            p = np.max(precision[recall >= t])
        ap = ap + p / 11.
    return ap
