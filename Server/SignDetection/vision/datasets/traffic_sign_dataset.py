import numpy as np
import logging
import pathlib
import xml.etree.ElementTree as ET
import cv2
import os


class TrafficSignDataset:
    def __init__(self,
                 root,
                 transform=None,
                 target_transform=None,
                 is_test=False,
                 shrink=False,
                 label_file=None):
        """Dataset for TS data.
        Args:
            root: the root of the dataset, the directory contains the following sub-directories:
                Annotations, ImageSets, JPEGImages, SegmentationClass, SegmentationObject.
        """
        self.root = pathlib.Path(root)
        self.transform = transform
        self.target_transform = target_transform
        self.shrink = shrink
        if is_test:
            if shrink:
                image_sets_file = self.root / "ImageSets_shrink/Main/test.txt"
            else:
                image_sets_file = self.root / "ImageSets/Main/test.txt"
        else:
            if shrink:
                image_sets_file = self.root / "ImageSets_shrink/Main/trainval.txt"
            else:
                image_sets_file = self.root / "ImageSets/Main/trainval.txt"
        self.ids = TrafficSignDataset._read_image_ids(image_sets_file)

        # if the labels file exists, read in the class names
        if shrink:
            label_file_name = self.root / "ImageSets_shrink/labels.txt"
        else:
            label_file_name = self.root / "labels.txt"

        if os.path.isfile(label_file_name):
            class_string = ""
            with open(label_file_name, 'r') as infile:
                for line in infile:
                    class_string += line.rstrip()

            # classes should be a , separated list

            classes = class_string.split(',')
            # prepend BACKGROUND as first class
            classes.insert(0, 'BACKGROUND')
            classes = [elem.lower().strip() for elem in classes]
            self.class_names = tuple(classes)
            logging.info("Labels read from file: " + str(self.class_names))

        else:
            logging.info("No labels file, using default classes.")
            self.class_names = ('BACKGROUND', 'aeroplane', 'bicycle', 'bird',
                                'boat', 'bottle', 'bus', 'car', 'cat', 'chair',
                                'cow', 'diningtable', 'dog', 'horse',
                                'motorbike', 'person', 'pottedplant', 'sheep',
                                'sofa', 'train', 'tvmonitor')

        self.class_dict = {
            class_name: i
            for i, class_name in enumerate(self.class_names)
        }

    def __getitem__(self, index):
        image_id = self.ids[index]
        boxes, labels = self._get_annotation(image_id)
        image = self._read_image(image_id)
        if self.transform:
            image, boxes, labels = self.transform(image, boxes, labels)
        if self.target_transform:
            gt_boxes = boxes
            gt_labels = labels
            boxes, labels = self.target_transform(gt_boxes, gt_labels)
        return image, boxes, labels

    def get_image(self, index):
        image_id = self.ids[index]
        image = self._read_image(image_id)
        if self.transform:
            image, _ = self.transform(image)
        return image

    def get_annotation(self, index):
        image_id = self.ids[index]
        return image_id, self._get_annotation(image_id)

    def __len__(self):
        return len(self.ids)

    @staticmethod
    def _read_image_ids(image_sets_file):
        ids = []
        with open(image_sets_file) as f:
            for line in f:
                ids.append(line.rstrip())
        return ids

    def _get_annotation(self, image_id):
        if self.shrink:
            annotation_file = self.root / f"Annotations_shrink/{image_id}.xml"
        else:
            annotation_file = self.root / f"Annotations/{image_id}.xml"
        objects = ET.parse(annotation_file).findall("object")
        boxes = []
        labels = []
        for object in objects:
            class_name = object.find('name').text.lower().strip()
            # Chá»‰ care bounding box classes trong annotaion
            if class_name in self.class_dict:
                bbox = object.find('bndbox')

                # dataset format theo Matlab, indexes bÄƒt dáº§u tá»« 0
                x1 = float(bbox.find('xmin').text) - 1
                y1 = float(bbox.find('ymin').text) - 1
                x2 = float(bbox.find('xmax').text) - 1
                y2 = float(bbox.find('ymax').text) - 1
                boxes.append([x1, y1, x2, y2])

                labels.append(self.class_dict[class_name])

        return (np.array(boxes,
                         dtype=np.float32), np.array(labels, dtype=np.int64))

    def _read_image(self, image_id):
        image_file = self.root / f"JPEGImages/{image_id}.jpg"
        image = cv2.imread(str(image_file))
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        return image
