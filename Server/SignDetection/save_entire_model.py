import torch
from vision.ssd.vgg_ssd import create_vgg_ssd
from vision.ssd.config import vgg_ssd_config
from vision.ssd.ssd import SSD as ssd
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from vision.ssd.mobilenet_v2_ssd_lite_jit_export import mobilenet_v2_ssd_lite


model = mobilenet_v2_ssd_lite(39)
model.load_state_dict(torch.load("F:\mb2.pth", map_location=torch.device('cpu')))
# Initialize optimizer
# model = torch.load("F:/mb2.pth", map_location=torch.device('cpu'))

model_jit = torch.jit.script(model)
torch.jit.save(model_jit, "F:\gg.pth")