from vision.ssd.vgg_ssd import create_vgg_ssd, create_vgg_ssd_predictor
from vision.ssd.mobilenet_v2_ssd_lite import create_mobilenetv2_ssd_lite, create_mobilenetv2_ssd_lite_predictor
from vision.ssd.mobilenet_v2_ssd_lite_jit import create_mobilenetv2_ssd_lite_jit, create_mobilenetv2_ssd_lite_jit_predictor
from vision.utils.misc import Timer
import cv2
import sys
import time

time.sleep(5) 

if len(sys.argv) < 5:
    print(
        'Usage: python run_ssd_example.py <net type>  <model path> <label path> <image path>'
    )
    sys.exit(0)
net_type = sys.argv[1]
model_path = sys.argv[2]
label_path = sys.argv[3]
image_path = sys.argv[4]

class_names = [name.strip() for name in open(label_path).readlines()]

if net_type == 'vgg16-ssd':
    net = create_vgg_ssd(len(class_names), is_test=True)
elif net_type == 'mb2-ssd-lite':
    net = create_mobilenetv2_ssd_lite(len(class_names), is_test=True)
elif net_type == 'mb2-ssd-lite-jit':
    net = create_mobilenetv2_ssd_lite_jit(len(class_names), is_test=True)
else:
    print("The net type is wrong. ")
    sys.exit(1)
net.load(model_path)

if net_type == 'vgg16-ssd':
    predictor = create_vgg_ssd_predictor(net, candidate_size=200)
elif net_type == 'mb2-ssd-lite':
    predictor = create_mobilenetv2_ssd_lite_predictor(net, candidate_size=200)
elif net_type == 'mb2-ssd-lite-jit':
    predictor = create_mobilenetv2_ssd_lite_jit_predictor(net,
                                                          candidate_size=200)
else:
    predictor = create_vgg_ssd_predictor(net, candidate_size=200)

orig_image = cv2.imread(image_path)
image = cv2.cvtColor(orig_image, cv2.COLOR_BGR2RGB)
boxes, labels, probs = predictor.predict(image, 10, 0.4)

for i in range(boxes.size(0)):
    try:
        box = boxes[i, :]
        xmin = int(box[0])
        ymin = int(box[1])
        xmax = int(box[2])
        ymax = int(box[3])

        cv2.rectangle(orig_image, (xmin, ymin), (xmax, ymax), (255, 255, 0), 4)
        #label = f"""{voc_dataset.class_names[labels[i]]}: {probs[i]:.2f}"""
        label = f"{class_names[labels[i]]}: {probs[i]:.2f}"
        print(label, ':', xmin, ',', ymin, ',', xmax, ',', ymax)
        cv2.putText(
            orig_image,
            label,
            (xmin + 20, ymin + 40),
            cv2.FONT_HERSHEY_SIMPLEX,
            1,  # font scale
            (255, 0, 255),
            2)  # line type
    except:
        continue
path = "run_ssd_example_output.jpg"
cv2.imwrite(path, orig_image)