from vision.ssd.vgg_ssd import create_vgg_ssd, create_vgg_ssd_predictor
from vision.ssd.mobilenet_v2_ssd_lite import create_mobilenetv2_ssd_lite, create_mobilenetv2_ssd_lite_predictor
from vision.utils.misc import Timer
import cv2
import sys
import io
import numpy as np
from PIL import Image
import base64
# from vision.utils.misc import Timer
import json
import time
import pymysql

from flask import Flask
from flask import request, jsonify
from flask_cors import CORS, cross_origin

#utils
# Take in base64 string and return PIL image
timer_predict = Timer()
timer_all = Timer()


def stringToImage(base64_string):
    imgdata = base64.b64decode(base64_string)
    return Image.open(io.BytesIO(imgdata))


# convert PIL Image to an RGB image( technically a numpy array ) that's compatible with opencv
def toRGB(image):
    return cv2.cvtColor(np.array(image), cv2.COLOR_BGR2RGB)


#file label và init net
label_path = "models/traffic-sign-classes.txt"
model_vgg_path = "models/ts.pth"
model_mb2_path = "models/ts_mb2.pth"
class_names = [name.strip() for name in open(label_path).readlines()]
net_vgg = create_vgg_ssd(len(class_names), is_test=True)
net_vgg.load(model_vgg_path)
predictor_vgg = create_vgg_ssd_predictor(net_vgg, candidate_size=200)

net_mb2 = create_mobilenetv2_ssd_lite(len(class_names), is_test=True)
net_mb2.load(model_mb2_path)
predictor_mb2 = create_mobilenetv2_ssd_lite_predictor(net_mb2,
                                                      candidate_size=200)

signData = {
    "100": "Biển báo cấm",
    "101": "Đường cấm",
    "102": "Cấm đi ngược chiều",
    "103a": "Cấm ô tô",
    "103b": "Cấm ô tô rẽ phải",
    "103c": "Cấm ô tô rẽ trái",
    "104": "Cấm môtô",
    "107": "Cấm ô tô khách và ô tô tải",
    "110": "Cấm xe đạp",
    "112": "Cấm người đi bộ",
    "115": "Hạn chế trọng lượng xe",
    "117": "Hạn chế chiều cao",
    "123a": "Cấm rẽ trái",
    "123b": "Cấm rẽ phải",
    "124a": "Cấm quay đầu",
    "127": "Tốc độ tối đa cho phép",
    "128": "Cấm bóp còi",
    "130": "Cấm dừng và đỗ xe",
    "131a": "Cấm đỗ xe",
    "200": "Biển báo nguy hiểm",
    "205": "Nơi giao nhau của đường đồng cấp",
    "207": "Giao nhau với đường không ưu tiên",
    "208": "Giao nhau với đường ưu tiên",
    "225": "Trẻ em",
    "245": "Đi chậm",
    "300": "Biển báo hiệu lệnh",
    "302a": "Hướng phải đi vòng sang phải",
    "302b": "Hướng phải đi vòng sang trái",
    "303": "Nơi giao nhau chạy theo vòng xuyến",
    "400": "Biển báo chỉ dẫn",
    "403": "Đường dành cho ô tô",
    "409": "Chỗ quay xe",
    "411": "Hướng đi theo vạch kẻ đường",
    "412": "Làn dành cho xe khách",
    "414": "Chỉ hướng đường",
    "415": "Chỉ hướng đi",
    "423": "Đường đi bộ",
    "424": "Cầu vượt qua đường",
    "500": "Biển phụ",
    "505": "Loại xe",
    "508": "Hướng rẽ",
    "509": "Chỗ đường sắt cắt đường bộ"
}

# Khai bao cong cua server
my_port = '51001'

# Doan ma khoi tao server
app = Flask(__name__)
app.config["TEMPLATES_AUTO_RELOAD"] = False
CORS(app)


# Khai bao ham xu ly request index
@app.route('/')
@cross_origin()
def index():
    return "Welcome to flask API!"


# Process post request /vgg16_ssd
@app.route('/vgg16-ssd', methods=['POST'])
@cross_origin()
def vgg16_ssd():
    timer_all.start("all")
    # Get uid
    uid = request.form.get('uid')
    # Get base64 of image sent from client
    img_base64 = request.form.get('img_base64')
    if img_base64:
        orig_image = stringToImage(img_base64)
        image = toRGB(orig_image)
    else:
        image_path = request.form.get('img_path')
        orig_image = cv2.imread(image_path)
        image = cv2.cvtColor(orig_image, cv2.COLOR_BGR2RGB)

    timer_predict.start("Predict")
    boxes, labels, probs = predictor_vgg.predict(image, 10, 0.2)
    print("Prediction: {:4f} seconds".format(timer_predict.end("Predict")))
    result = []
    response = []
    for i in range(boxes.size(0)):
        # try:
        box = boxes[i, :]
        xmin = int(box[0])
        ymin = int(box[1])
        xmax = int(box[2])
        ymax = int(box[3])
        result += [{"label": class_names[labels[i]], "sign": signData[class_names[labels[i]]], "prob": f"{probs[i]:.2f}", "xmin": xmin, "ymin": ymin, "xmax": xmax, "ymax": ymax}]

    time_exec = timer_all.end("all")
    print("Prediction all: {:4f} seconds".format(time_exec))
    if len(result) > 0:
        maxId, maxProb = 0, 0
        for i in range(len(result)):
            if maxProb < float(result[i]["prob"]):
                maxProb = float(result[i]["prob"])
                maxId = i 
        response = [result[maxId]]
        # Save image
        ts = int(time.time())
        box_image = cv2.rectangle(image, (response[0]["xmin"], response[0]["ymin"]), (response[0]["xmax"], response[0]["ymax"]), (0, 0, 255), 10)
        cv2.imwrite("../Resource/Sign/" + str(ts) + ".jpg", image)
        # Insert DB
        conn = pymysql.connect(host='localhost', user='root', password='', charset='utf8', db='thibanglaixe')
        cur = conn.cursor()
        sqlCmd = "INSERT INTO sign (uid, time, image, title) VALUES(%s, %s, %s, %s)"
        cur.execute(sqlCmd, (uid, str(ts), str(ts) + ".jpg", response[0]["sign"]))
        conn.commit()
    # Return 
    print(str(response))
    return str(response)

@app.route('/ping', methods=['GET'])
# @cross_origin()
def pong():
    return "pong"


# Thuc thi server
if __name__ == '__main__':
    app.run(debug = True, host = '0.0.0.0', port = my_port)
