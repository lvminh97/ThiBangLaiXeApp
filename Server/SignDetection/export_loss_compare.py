from matplotlib import pyplot as plt
import pickle
import numpy as np
import sys
import time

if len(sys.argv) < 3:
    print(
        'Usage: python export_loss_func.py <loss vgg>  <loss mb2> (Mac dinh VGG o truoc)'
    )
    sys.exit(0)
fp_vgg = open(sys.argv[1], 'rb')
losses_vgg = pickle.load(fp_vgg)
epoch_vgg = int(len(losses_vgg[0]))
print("VGG Epoc:" + str(epoch_vgg))
avg_loss_vgg = losses_vgg[0]
reg_loss_vgg = losses_vgg[1]
conf_loss_vgg = losses_vgg[2]
epoch_vgg = np.linspace(0, epoch_vgg, epoch_vgg)

fp_mb2 = open(sys.argv[2], 'rb')
losses_mb2 = pickle.load(fp_mb2)
epoch_mb2 = int(len(losses_mb2[0]))
print("Mb2 Epoc:" + str(epoch_mb2))
avg_loss_mb2 = losses_mb2[0]
reg_loss_mb2 = losses_mb2[1]
conf_loss_mb2 = losses_mb2[2]
epoch_mb2 = np.linspace(0, epoch_mb2, epoch_mb2)
###############
plt.figure()
plt.plot(epoch_vgg, np.array(avg_loss_vgg))
plt.plot(epoch_mb2, np.array(avg_loss_mb2))
plt.title("Multibox Loss VGG16-SSD vs MobileNetV2 SSD Lite")
plt.legend(["VGG16-SSD", "MobileNetV2 SSD Lite"])
plt.ylabel("Loss")
plt.xlabel("Epoch")
plt.savefig(
    f'models/export_loss/LossVGG_MB2{int(round(time.time() * 1000))}.png')
###############
plt.figure()
plt.plot(epoch_vgg, np.array(reg_loss_vgg))
plt.plot(epoch_mb2, np.array(reg_loss_mb2))
plt.title("Regression Loss VGG16-SSD vs MobileNetV2 SSD Lite")
plt.legend(["VGG16-SSD", "MobileNetV2 SSD Lite"])
plt.ylabel("Loss")
plt.xlabel("Epoch")
plt.savefig(
    f'models/export_loss/LocLossVGG_MB2{int(round(time.time() * 1000))}.png')
###############
plt.figure()
plt.plot(epoch_vgg, np.array(conf_loss_vgg))
plt.plot(epoch_mb2, np.array(conf_loss_mb2))
plt.title("Confidence Loss VGG16-SSD vs MobileNetV2 SSD Lite")
plt.legend(["VGG16-SSD", "MobileNetV2 SSD Lite"])
plt.ylabel("Loss")
plt.xlabel("Epoch")
plt.savefig(
    f'models/export_loss/ConfLossVGG_MB2{int(round(time.time() * 1000))}.png')
