from vision.ssd.vgg_ssd import create_vgg_ssd, create_vgg_ssd_predictor
from vision.ssd.mobilenet_v2_ssd_lite import create_mobilenetv2_ssd_lite, create_mobilenetv2_ssd_lite_predictor
from vision.ssd.mobilenet_v2_ssd_lite_jit import create_mobilenetv2_ssd_lite_jit, create_mobilenetv2_ssd_lite_jit_predictor
from vision.ssd.mobilenet_v2_ssd_lite_jit_export import mobilenet_v2_ssd_lite as export_model
from vision.utils.misc import Timer
import cv2
import sys

if len(sys.argv) < 4:
    print(
        'Usage: python run_ssd_example.py <net type>  <model path> <label path> [video file]'
    )
    sys.exit(0)
net_type = sys.argv[1]
model_path = sys.argv[2]
label_path = sys.argv[3]

if len(sys.argv) >= 5:
    cap = cv2.VideoCapture(sys.argv[4])  # capture from file
else:
    cap = cv2.VideoCapture(0)  # capture from camera
    cap.set(3, 1920)
    cap.set(4, 1080)

class_names = [name.strip() for name in open(label_path).readlines()]
num_classes = len(class_names)

if net_type == 'vgg16-ssd':
    net = create_vgg_ssd(len(class_names), is_test=True)
elif net_type == 'mb2-ssd-lite':
    net = create_mobilenetv2_ssd_lite(len(class_names), is_test=True)
elif net_type == 'mb2-ssd-lite-jit':
    net = create_mobilenetv2_ssd_lite_jit(len(class_names), is_test=True)
else:
    print("The net type is wrong.")
    sys.exit(1)
net.load(model_path)

if net_type == 'vgg16-ssd':
    predictor = create_vgg_ssd_predictor(net, candidate_size=200)
elif net_type == 'mb2-ssd-lite':
    predictor = create_mobilenetv2_ssd_lite_predictor(net, candidate_size=200)
elif net_type == 'mb2-ssd-lite-jit':
    predictor = create_mobilenetv2_ssd_lite_jit_predictor(net,
                                                          candidate_size=200)
else:
    print("The net type is wrong. ")
    sys.exit(1)

timer = Timer()
while True:
    ret, orig_image = cap.read()
    if orig_image is None:
        continue
    image = cv2.cvtColor(orig_image, cv2.COLOR_BGR2RGB)
    timer.start()
    boxes, labels, probs = predictor.predict(image, 10, 0.1)
    interval = timer.end()
    print('Time: {:.2f}s, TnoS Detect Objects: {:d}.'.format(
        interval, labels.size(0)))
    for i in range(boxes.size(0)):
        box = boxes[i, :]
        label = f"{class_names[labels[i]]}: {probs[i]:.2f}"
        xmin = int(box[0])
        ymin = int(box[1])
        xmax = int(box[2])
        ymax = int(box[3])

        #cv2.rectangle(orig_image, (box[0], box[1]), (box[2], box[3]), (255, 255, 0), 4)
        cv2.rectangle(orig_image, (xmin, ymin), (xmax, ymax), (255, 255, 0), 4)
        cv2.putText(
            orig_image,
            label,
            (xmin + 20, ymin + 40),
            cv2.FONT_HERSHEY_SIMPLEX,
            1,  # font scale
            (255, 0, 255),
            2)  # line type
    cv2.imshow('annotated', orig_image)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
cap.release()
cv2.destroyAllWindows()
