from flask import Flask, render_template
from flask_socketio import SocketIO, emit
from vision.ssd.vgg_ssd import create_vgg_ssd, create_vgg_ssd_predictor
from vision.ssd.mobilenet_v2_ssd_lite import create_mobilenetv2_ssd_lite, create_mobilenetv2_ssd_lite_predictor
from vision.utils.misc import Timer
import cv2
import sys
import io
import numpy as np
from PIL import Image
import base64
from vision.utils.misc import Timer
from io import BytesIO

#utils
# Take in base64 string and return PIL image
timer_predict = Timer()
timer_all = Timer()


def stringToImage(base64_string):
    imgdata = base64.b64decode(base64_string)
    return Image.open(io.BytesIO(imgdata))


# convert PIL Image to an RGB image( technically a numpy array ) that's compatible with opencv
def toRGB(image):
    return cv2.cvtColor(np.array(image), cv2.COLOR_BGR2RGB)


#file label và init net
label_path = "models/ts.txt"
model_vgg_path = "models/ts.pth"
model_mb2_path = "models/ts_mb2.pth"
class_names = [name.strip() for name in open(label_path).readlines()]
net_vgg = create_vgg_ssd(len(class_names), is_test=True)
net_vgg.load(model_vgg_path)
predictor_vgg = create_vgg_ssd_predictor(net_vgg, candidate_size=200)

net_mb2 = create_mobilenetv2_ssd_lite(len(class_names), is_test=True)
net_mb2.load(model_mb2_path)
predictor_mb2 = create_mobilenetv2_ssd_lite_predictor(net_mb2,
                                                      candidate_size=200)

app = Flask(__name__)

app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)


@app.route('/')
def index():
    # print("inedx")
    return render_template('index.html')


@socketio.on('vgg16-ssd')
def detect_by_vgg16(img_bytes, threshold):
    timer_all.start("all")
    orig_image = cv2.imdecode(np.frombuffer(img_bytes, np.uint8),cv2.IMREAD_COLOR)
    image = toRGB(orig_image)
    timer_predict.start("Predict")
    boxes, labels, probs = predictor_vgg.predict(image, 10, threshold)
    print("Prediction: {:4f} seconds".format(timer_predict.end("Predict")))
    result = ""
    for i in range(boxes.size(0)):
        # try:
        box = boxes[i, :]
        xmin = int(box[0])
        ymin = int(box[1])
        xmax = int(box[2])
        ymax = int(box[3])
        label = f"{class_names[labels[i]]}: {probs[i]:.2f}"
        result += "\r\n" + label + ':' + str(xmin) + ',' + str(
            ymin) + ',' + str(xmax) + ',' + str(ymax)
        #print(label, ':', xmin, ',', ymin, ',', xmax, ',', ymax)
    time_exec = timer_all.end("all")
    print("Prediction all: {:4f} seconds".format(time_exec))
    result = "Inference time:" + str(time_exec) + result
    emit("response", result)


@socketio.on('mb2-ssd-lite')
def detect_by_mb2(img_bytes,threshold):
    timer_all.start("all")
    orig_image = cv2.imdecode(np.frombuffer(img_bytes, np.uint8),cv2.IMREAD_COLOR)
    image = toRGB(orig_image)

    timer_predict.start("Predict")
    boxes, labels, probs = predictor_mb2.predict(image, 10, threshold)
    print("Prediction: {:4f} seconds".format(timer_predict.end("Predict")))
    result = ""
    for i in range(boxes.size(0)):
        # try:
        box = boxes[i, :]
        xmin = int(box[0])
        ymin = int(box[1])
        xmax = int(box[2])
        ymax = int(box[3])
        label = f"{class_names[labels[i]]}: {probs[i]:.2f}"
        result += "\r\n" + label + ':' + str(xmin) + ',' + str(
            ymin) + ',' + str(xmax) + ',' + str(ymax)
        #print(label, ':', xmin, ',', ymin, ',', xmax, ',', ymax)
    time_exec = timer_all.end("all")
    print("Prediction all: {:4f} seconds".format(time_exec))
    result = "Inference time:" + str(time_exec) + result
    emit("response", result)


@socketio.on("ping-data")
def pong(img_base64):
    print(len(img_base64))
    emit("ping", "pong")


@socketio.on("ping")
def pongp():
    print("ting ting")
    emit("ping", "pong")


if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0', port=51002)