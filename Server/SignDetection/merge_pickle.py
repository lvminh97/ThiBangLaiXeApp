from matplotlib import pyplot as plt
import pickle
import numpy as np
import sys
import time

m1 = open('models/log/mb2-ssd-lite-jitloss_071120_1.pickle', 'rb')
losses1 = pickle.load(m1)
m2 = open('models/log/mb2-ssd-lite-jitloss_071120_2.pickle', 'rb')
losses2 = pickle.load(m2)
m3 = open('models/log/mb2-ssd-lite-jitloss_071120_3.pickle', 'rb')
losses3 = pickle.load(m3)
m4 = open('models/log/mb2-ssd-lite-jitloss_071120_4.pickle', 'rb')
losses4 = pickle.load(m4)


avg_losses = losses1[0] + losses2[0] + losses3[0] + losses4[0] 
reg_losses = losses1[1] + losses2[1] + losses3[1] + losses4[1] 
conf_losses = losses1[2] + losses2[2] + losses3[2] + losses4[2] 
losses = []
losses.append(avg_losses)
losses.append(reg_losses)
losses.append(conf_losses)

print(len(losses[0]))
pickle_out = open('models/log/mb2.pickle', "wb")
pickle.dump(losses, pickle_out)