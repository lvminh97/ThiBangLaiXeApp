from matplotlib import pyplot as plt
import pickle
import numpy as np
import sys
import time

if len(sys.argv) < 3:
    print('Usage: python export_loss_func.py <pickle file path>  <title>')
    sys.exit(0)
fp = open(sys.argv[1], 'rb')
losses = pickle.load(fp)
epoch = int(len(losses[0]))
avg_loss = losses[0]
reg_loss = losses[1]
conf_loss = losses[2]
epoch = np.linspace(0, epoch, epoch)
plt.plot(epoch, np.array(avg_loss))
plt.plot(epoch, np.array(reg_loss))
plt.plot(epoch, np.array(conf_loss))
plt.title(sys.argv[2])
plt.legend(["MultiBox Loss", "Localization Loss", "Confidence Loss"])
plt.ylabel("Loss")
plt.xlabel("Epoch")
plt.savefig(
    f'models/export_loss/{sys.argv[2]}{int(round(time.time() * 1000))}.png')
