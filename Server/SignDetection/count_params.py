from prettytable import PrettyTable
from vision.ssd.vgg_ssd import create_vgg_ssd, create_vgg_ssd_predictor
from vision.ssd.mobilenet_v2_ssd_lite import create_mobilenetv2_ssd_lite, create_mobilenetv2_ssd_lite_predictor
from vision.ssd.mobilenet_v2_ssd_lite_jit import create_mobilenetv2_ssd_lite_jit, create_mobilenetv2_ssd_lite_jit_predictor

net_vgg = create_vgg_ssd(39, is_test=True)
net_mb2 = create_mobilenetv2_ssd_lite(39, is_test=True)


def count_parameters(model, name_net='NET SSD'):
    table = PrettyTable(["Modules", "Parameters"])
    total_params = 0
    for name, parameter in model.named_parameters():
        if not parameter.requires_grad: continue
        param = parameter.numel()
        table.add_row([name, param])
        total_params += param
    print(table)
    print(f"{name_net} : Total Trainable Params of: {total_params}")
    return total_params


count_parameters(net_vgg, "VGG16-SSD")
count_parameters(net_mb2, "MobileNetV2-SSD-Lite")